readme.txt file for the online submission procedure.
CPSC 303 2012W2.

--------------------------------------------------------------------------
2012-03-21 (Ian Mitchell)

This directory contains the files for Homework #6:

hw-6.pdf: The assignment file.

converge.m: A script to generate an empirical convergence plot for
specified fixed timestep integrators on a suitable test problem.

forwardEuler.m: An implementation of the forward Euler scheme to be
used with converge.

rk4_buggy.m: A (buggy) implementation of the classical 4th order
accuracy Runge-Kutta scheme to be used with converge.

To complete an online submission, follow the directions at the
course's homework web page:

https://sites.google.com/site/ubccpsc303winter2012/homeworks

--------------------------------------------------------------------------
