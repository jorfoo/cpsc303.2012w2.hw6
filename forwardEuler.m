function [ t, y ] = forwardEuler(odefunc, tspan, yinit, stepSize)
%
%   [ t, y ] = forwardEuler(func, tspan, yinit, stepSize)
%
% Implements the forward euler time integration for an ODE IVP
%   with fixed stepsize.
%
% The first three input parameters and both output parameters
%   are the same as used for Matlab's ODE suite.
%
% Parameters:
%    odefunc    Function with prototype dy = f(t,y).
%    tspan      Two element row vector specifying start and end times.
%    yinit      Initial condition column vector.
%    stepSize   Fixed stepsize to be used in the forward Euler steps.
%
%    t          Column vector of sample times.
%    y          Matrix of solution values at sample times.
%                 Each row corresponds to the solution value at the
%                 corresponding sample time in t.
%
% Ian Mitchell for CPSC 403, 2005/01/14.

% Set up the outputs.
numSteps = ceil((tspan(2) - tspan(1)) / stepSize) + 1;
t = zeros(numSteps, 1);
y = zeros(numSteps, length(yinit));

% Initial conditions.
tNow = tspan(1);
yNow = yinit;

% Record the step.
step = 1;
t(step) = tNow;
y(step,:) = yNow';

% Integrate.
for step = 2 : numSteps

  % The size of the final step may need to be adjusted to hit
  %   the end of the integration interval.
  if(step < numSteps)
    h = stepSize;
  else
    h = tspan(2) - tNow;
  end
  
  % Make new step.
  yNow = yNow + h * feval(odefunc, tNow, yNow);
  tNow = tNow + h;

  % Record the step.
  t(step) = tNow;
  y(step,:) = yNow';

end
