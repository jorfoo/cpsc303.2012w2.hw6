function [ t, y ] = rk4(odefunc, tspan, yinit, stepSize)
%
%   [ t, y ] = rk4(func, tspan, yinit, stepSize)
%
% Implements the classical RK4 time integration for an ODE IVP
%   with fixed stepsize.
%
% The first three input parameters and both output parameters
%   are the same as used for Matlab's ODE suite.
%
% Parameters:
%    odefunc    Function with prototype dy = f(t,y).
%    tspan      Two element row vector specifying start and end times.
%    yinit      Initial condition column vector.
%    stepSize   Fixed stepsize to be used in the forward Euler steps.
%
%    t          Column vector of sample times.
%    y          Matrix of solution values at sample times.
%                 Each row corresponds to the solution value at the
%                 corresponding sample time in t.
%
% Ian Mitchell for CPSC 303, 2006/03/24.

% Section 0: Scheme Parameters
substeps = 4;
A = [   0,   0, 0, 0; ...
      0.5,   0, 0, 0; ...
      0.5,   0, 0, 0; ...
        1,   0, 0, 0 ];
b = (1/4) * [ 1; 1; 1; 1 ];
c = [ 0; 0.5; 0.5; 1 ];
  
% Section 1: Set up the outputs.
numSteps = ceil((tspan(2) - tspan(1)) / stepSize) + 1;
t = zeros(numSteps, 1);
y = zeros(numSteps, length(yinit));

% Section 2: Initial conditions.
tNow = tspan(1);
yNow = yinit;

% Section 3: Record the step.
step = 1;
t(step) = tNow;
y(step,:) = yNow';

% Integrate.
for step = 2 : numSteps

  % Section 4: The size of the final step may need to be adjusted to hit
  %   the end of the integration interval.
  if(step < numSteps)
    h = stepSize;
  else
    h = tspan(2) - tNow;
  end
  
  % Section 5: Make new step.
  intermediate = zeros(length(yinit), substeps);
  yNext = yNow;
  for s1 = 1 : substeps
    intermediate(:,s1) = yNow;
    
    for s2 = 1 : substeps
      if(A(s1, s2) ~= 0)
        f_intermediate = feval(odefunc, tNow + c(s2) * h, intermediate(:,s1));
        intermediate(:,s1) = intermediate(:,s1) + (h*A(s1,s2)*f_intermediate);
      end
    end

    f_intermediate = feval(odefunc, tNow + c(s1) * h, intermediate(:,s1));
    yNext = yNext + h * b(s1) * f_intermediate;
  end
  
  tNow = tNow + h;
  yNow = yNext;

  % Section 6: Record the step.
  t(step) = tNow;
  y(step,:) = yNow';

end
