function [t, y] = bdf2 (odeFunc, tspan, yinit, stepSize)
% Set up the outputs.
numSteps = ceil((tspan(2) - tspan(1)) / stepSize) + 1;
numSteps = 4;
t = zeros(numSteps, 1);
y = zeros(numSteps, length(yinit));

% Initial conditions.
tNow = tspan(1);
yNow = yinit;

% Record the two steps.
step = 1;
t(step) = tNow;
y(step,:) = feval(odeFunc,tNow, yNow)';

% Integrate.
for step = 2 : numSteps
  
  % The size of the final step may need to be adjusted to hit
  %   the end of the integration interval.
  if(step < numSteps)
    h = stepSize;
  else
    h = tspan(2) - tNow;
  end
  
  % Make new step.
  if step == 2
    y(step,:) = feval(odeFunc,tNow+h, yNow + h * y(1, :));
  else
    y(step,:) = 1/3*(4*y(step-1,:)-y(step-2,:)+2*h*feval(odeFunc,tNow, yNow));
  end
  tNow = tNow + h;

  % Record the step.
  t(step) = tNow;
  y(step) = yNow';
end