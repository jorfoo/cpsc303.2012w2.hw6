yA0 = [0.2 0.2];
yB0 = [1.2 0];
[tA, yA] = ode45('dydt', [0 20], yA0);
[tB, yB] = ode45('dydt', [0 20], yB0);

%time plots
subplot(2,2,1), plot(tA,yA(1:end,1), 'b.:',tA,yA(1:end,2), 'g.:');
xlabel('time t');
ylabel('value y');
title('ODE45 solution of yA - t vs y - Jordan Foo - 12186086 - CPSC 303 HW6Q2');

subplot(2,2,2), plot(tB,yB(1:end,1), 'b.:',tB,yB(1:end,2), 'g.:');
xlabel('time t');
ylabel('value y');
title('ODE45 solution of yB - t vs y - Jordan Foo - 12186086 - CPSC 303 HW6Q2');

%phase plots
subplot(2,2,3), plot(yA(1:end, 1),yA(1:end,2));
xlabel('time t');
ylabel('value y');
title('ODE45 solution of yA - y1 vs y2 - Jordan Foo - 12186086 - CPSC 303 HW6Q2');

subplot(2,2,4), plot(yB(1:end, 1),yB(1:end,2));
xlabel('y1');
ylabel('y2');
title('ODE45 solution of yB - y1 vs y2 - Jordan Foo - 12186086 - CPSC 303 HW6Q2');