function dydt = dydt(t,y)
dydt = zeros(size(y));
dydt(1) = -y(2) + y(1) * (1-tanh(t-10))*(1-y(1).^2);
dydt(2) = y(1) + y(2) * (1-tanh(t-10))*(1-y(2).^2);