% converge: show convergence plot of fixed timestep schemes.
%
% Investigates the convergence rates of fixed timestep schemes on a simple
% ODE.
%
% No Parameters.
%
% Ian Mitchell for CPSC 303, 2013/03/21.

% Plot each solution as it is generated?
plot_solns = false;

% What schemes?
schemes = { @forwardEuler; @rk4_buggy; @rk4_correct; @bdf2 };
num_schemes = length(schemes);
% For creating the legend.
scheme_names = cell(size(schemes));
for i = 1 : num_schemes
  scheme_names{i} = func2str(schemes{i});
end

% Base timestep (this should ideally be exactly represented in floating point).
h_base = 0.5;
h_const = 1.0;

% Timestep powers (integers).
h_powers = 2:10;
%hPowers = 2:15;
num_powers = length(h_powers);

% ODE and analytic solution.
int_func = @(t, y) (t + 2 * t * y);
y0 = 0;
soln_func = @(t) (0.5 * (exp(t.^2) - 1));

% IVP parameters.
tspan = [ 0, 2 ];

% For plotting solutions.
t_soln = linspace(tspan(1), tspan(2), 1001);
y_soln = feval(soln_func, t_soln);

% How many grid points?
ns = (tspan(2) - tspan(1)) ./ (h_const * h_base .^ h_powers);
hs = h_const * h_base .^ h_powers;

% For recording the errors for each discretization and scheme.
errs_exact = zeros(num_powers, num_schemes);

for i = 1 : num_schemes
  fprintf('\nWorking on scheme: %s', scheme_names{i});
  for j = 1 : num_powers

    % Error for h such that discontinuity falls on a timestep boundary.
    h = hs(j);
    [ t_exact y_exact ] = feval(schemes{i}, int_func, tspan, y0, h);
    y_true = feval(soln_func, t_exact);
    errs_exact(j,i) = abs(y_exact(end) - y_soln(end));
    %errs_exact(j,i) = sum(abs(y_exact - y_true)) / length(t_exact);

    % Plot solution (if necessary).
    if(plot_solns)
      hold off; %#ok<UNRCH>
      plot(t_soln, y_soln, 'k-');
      hold on;
      plot(t_exact, y_exact, 'b-+');
      title([ func2str(schemes{i}) ', h = ' num2str(h) ]);
      pause;
    end

  end
end

% Plot empirical convergence curve, and some lines of fixed slope.
hold off;
h_line = loglog(hs, errs_exact);
hold on;
loglog([ max(hs), max(hs) / 100 ], ...
       [ errs_exact(1,1), errs_exact(1,1) * 0.01 ], 'k-');
loglog([ max(hs), max(hs) / 100 ], ...
       [ errs_exact(1,1), errs_exact(1,1) * 0.01^2 ], 'k-');
loglog([ max(hs), max(hs) / 100 ], ...
       [ errs_exact(1,1), errs_exact(1,1) * 0.01^4 ], 'k-');
xlabel('Stepsize');

% Make the plot pretty.
styles = { ':', '--', '-', '-.' };
symbols = { '+', 'o', 'x', 'v' };
axis tight;
ylabel('Error');
title('Empirical Convergence Rates');
h_legend = legend(scheme_names);
% Make sure that underscore characters are not misinterpreted in the legend.
set(h_legend, 'Interpreter', 'None');
for i = 1 : num_schemes
  set(h_line(i), 'LineWidth', 2, 'LineStyle', styles{i}, 'Marker', symbols{i});
end

% Add a newline at the end so that the Matlab prompt has its own line.
fprintf('\n\n');
